#include "DroneIBVSController.h"


DroneIBVSController::DroneIBVSController()
{

    return;
}


bool DroneIBVSController::readConfigs(std::string configFile)
{

    try
    {
        XMLFileReader my_xml_reader(configFile);

        MULTIROTOR_IBVSCONTROLLER_DYAWMAX   = my_xml_reader.readDoubleValue("ibvs_controller_config:output_saturations:MULTIROTOR_IBVSCONTROLLER_DYAWMAX");
        MULTIROTOR_IBVSCONTROLLER_DZMAX     = my_xml_reader.readDoubleValue("ibvs_controller_config:output_saturations:MULTIROTOR_IBVSCONTROLLER_DZMAX");
        MULTIROTOR_IBVSCONTROLLER_MAX_PITCH = my_xml_reader.readDoubleValue("ibvs_controller_config:output_saturations:MULTIROTOR_IBVSCONTROLLER_MAX_PITCH");
        MULTIROTOR_IBVSCONTROLLER_MAX_ROLL  = my_xml_reader.readDoubleValue("ibvs_controller_config:output_saturations:MULTIROTOR_IBVSCONTROLLER_MAX_ROLL");

        MULTIROTOR_TILT_REFERENCE_CUTOFF_TR = my_xml_reader.readDoubleValue("ibvs_controller_config:low_pass_filter_configs:MULTIROTOR_TILT_REFERENCE_CUTOFF_TR");
        MULTIROTOR_DYAW_REFERENCE_CUTOFF_TR = my_xml_reader.readDoubleValue("ibvs_controller_config:low_pass_filter_configs:MULTIROTOR_DYAW_REFERENCE_CUTOFF_TR");
        MULTIROTOR_DALT_REFERENCE_CUTOFF_TR = my_xml_reader.readDoubleValue("ibvs_controller_config:low_pass_filter_configs:MULTIROTOR_DALT_REFERENCE_CUTOFF_TR");

        MULTIROTOR_FRONTCAM_RESOLUTION_WIDTH          = my_xml_reader.readDoubleValue("ibvs_controller_config:camera_constants:MULTIROTOR_FRONTCAM_RESOLUTION_WIDTH");
        MULTIROTOR_FRONTCAM_RESOLUTION_HEIGHT         = my_xml_reader.readDoubleValue("ibvs_controller_config:camera_constants:MULTIROTOR_FRONTCAM_RESOLUTION_HEIGHT");
        MULTIROTOR_FRONTCAM_HORIZONTAL_ANGLE_OF_VIEW  = my_xml_reader.readDoubleValue("ibvs_controller_config:camera_constants:MULTIROTOR_FRONTCAM_HORIZONTAL_ANGLE_OF_VIEW");
        MULTIROTOR_FRONTCAM_VERTICAL_ANGLE_OF_VIEW    = my_xml_reader.readDoubleValue("ibvs_controller_config:camera_constants:MULTIROTOR_FRONTCAM_VERTICAL_ANGLE_OF_VIEW");
        MULTIROTOR_FRONTCAM_ALPHAX                    = my_xml_reader.readDoubleValue("ibvs_controller_config:camera_constants:MULTIROTOR_FRONTCAM_ALPHAX");
        MULTIROTOR_FRONTCAM_ALPHAY                    = my_xml_reader.readDoubleValue("ibvs_controller_config:camera_constants:MULTIROTOR_FRONTCAM_ALPHAY");
        MULTIROTOR_IBVSCONTROLLER_INIT_DEPTH          = my_xml_reader.readDoubleValue("ibvs_controller_config:camera_constants:MULTIROTOR_IBVSCONTROLLER_INIT_DEPTH");
        MULTIROTOR_IBVSCONTROLLER_TARGET_INIT_SIZE    = my_xml_reader.readDoubleValue("ibvs_controller_config:camera_constants:MULTIROTOR_IBVSCONTROLLER_TARGET_INIT_SIZE");
        MULTIROTOR_FAERO_DCGAIN_SPEED2TILT            = my_xml_reader.readDoubleValue("ibvs_controller_config:camera_constants:MULTIROTOR_FAERO_DCGAIN_SPEED2TILT");

        MULTIROTOR_FRONTCAM_C_fx2Dy = ( MULTIROTOR_IBVSCONTROLLER_INIT_DEPTH*MULTIROTOR_FRONTCAM_RESOLUTION_WIDTH/(MULTIROTOR_FRONTCAM_ALPHAX) );
        MULTIROTOR_FRONTCAM_C_fx2DY = ( MULTIROTOR_FRONTCAM_HORIZONTAL_ANGLE_OF_VIEW * (M_PI/180.0) );
        MULTIROTOR_FRONTCAM_C_fy2Dz = ( MULTIROTOR_IBVSCONTROLLER_INIT_DEPTH*MULTIROTOR_FRONTCAM_RESOLUTION_HEIGHT/(MULTIROTOR_FRONTCAM_ALPHAY) );
        MULTIROTOR_FRONTCAM_C_fD2Dx = ( sqrt( (MULTIROTOR_FRONTCAM_ALPHAX*MULTIROTOR_FRONTCAM_ALPHAY*MULTIROTOR_IBVSCONTROLLER_TARGET_INIT_SIZE)/(MULTIROTOR_FRONTCAM_RESOLUTION_WIDTH*MULTIROTOR_FRONTCAM_RESOLUTION_HEIGHT) ) );
        MULTIROTOR_FRONTCAM_C_DY2Dfx= ( 1/(MULTIROTOR_FRONTCAM_C_fx2DY) );
        MULTIROTOR_FRONTCAM_C_DP2Dfy= ( 1/( MULTIROTOR_FRONTCAM_VERTICAL_ANGLE_OF_VIEW * (M_PI/180.0) ) );

        MULTIROTOR_IBVSCONTROLLER_FX2R_DELTA_KP = my_xml_reader.readDoubleValue("ibvs_controller_config:fx2r:FX2R_DELTA_KP");
        FX2R_KP                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fx2r:FX2R_KP");
        FX2R_KI                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fx2r:FX2R_KI");
        FX2R_KD                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fx2r:FX2R_KD");

        MULTIROTOR_IBVSCONTROLLER_FX2R_KP = (((FX2R_KP) * MULTIROTOR_FRONTCAM_C_fx2Dy  * MULTIROTOR_IBVSCONTROLLER_FX2R_DELTA_KP ) * MULTIROTOR_FAERO_DCGAIN_SPEED2TILT );
        MULTIROTOR_IBVSCONTROLLER_FX2R_KI = (( FX2R_KI) * MULTIROTOR_IBVSCONTROLLER_FX2R_KP );
        MULTIROTOR_IBVSCONTROLLER_FX2R_KD = (( FX2R_KD) * MULTIROTOR_IBVSCONTROLLER_FX2R_KP );

        MULTIROTOR_IBVSCONTROLLER_FD2P_DELTA_KP = my_xml_reader.readDoubleValue("ibvs_controller_config:fd2p:FD2P_DELTA_KP");
        FD2P_KP                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fd2p:FD2P_KP");
        FD2P_KI                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fd2p:FD2P_KI");
        FD2P_KD                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fd2p:FD2P_KD");

        MULTIROTOR_IBVSCONTROLLER_FD2P_KP = (((FD2P_KP) * MULTIROTOR_FRONTCAM_C_fD2Dx * MULTIROTOR_IBVSCONTROLLER_FD2P_DELTA_KP ) * MULTIROTOR_FAERO_DCGAIN_SPEED2TILT );
        MULTIROTOR_IBVSCONTROLLER_FD2P_KI =  ((FD2P_KI) * MULTIROTOR_IBVSCONTROLLER_FD2P_KP );
        MULTIROTOR_IBVSCONTROLLER_FD2P_KD =  ((FD2P_KD) * MULTIROTOR_IBVSCONTROLLER_FD2P_KP );

        MULTIROTOR_IBVSCONTROLLER_FX2DY_DELTA_KP = my_xml_reader.readDoubleValue("ibvs_controller_config:fx2dy:FX2DY_DELTA_KP");
        FX2DY_KP                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fx2dy:FX2DY_KP");
        FX2DY_KI                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fx2dy:FX2DY_KI");
        FX2DY_KD                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fx2dy:FX2DY_KD");

        MULTIROTOR_IBVSCONTROLLER_FX2DY_KP = ((FX2DY_KP) * MULTIROTOR_FRONTCAM_C_fx2DY * MULTIROTOR_IBVSCONTROLLER_FX2DY_DELTA_KP );
        MULTIROTOR_IBVSCONTROLLER_FX2DY_KI = ((FX2DY_KI) * MULTIROTOR_IBVSCONTROLLER_FX2DY_KP );
        MULTIROTOR_IBVSCONTROLLER_FX2DY_KD = ((FX2DY_KD) * MULTIROTOR_IBVSCONTROLLER_FX2DY_KP );

        MULTIROTOR_IBVSCONTROLLER_FY2DZ_DELTA_KP = my_xml_reader.readDoubleValue("ibvs_controller_config:fy2dz:FY2DZ_DELTA_KP");
        FY2DZ_KP                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fy2dz:FY2DZ_KP");
        FY2DZ_KI                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fy2dz:FY2DZ_KI");
        FY2DZ_KD                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fy2dz:FY2DZ_KD");

        MULTIROTOR_IBVSCONTROLLER_FY2DZ_KP = ((FY2DZ_KP) * MULTIROTOR_FRONTCAM_C_fy2Dz * MULTIROTOR_IBVSCONTROLLER_FY2DZ_DELTA_KP );
        MULTIROTOR_IBVSCONTROLLER_FY2DZ_KI = ((FY2DZ_KI) * MULTIROTOR_IBVSCONTROLLER_FY2DZ_KP );
        MULTIROTOR_IBVSCONTROLLER_FY2DZ_KD = ((FY2DZ_KD) * MULTIROTOR_IBVSCONTROLLER_FY2DZ_KP );

        MULTIROTOR_IBVSCONTROLLER_FS2P_DELTA_KP = my_xml_reader.readDoubleValue("ibvs_controller_config:fs2p:FS2P_DELTA_KP");
        FS2P_KP                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fs2p:FS2P_KP");
        FS2P_KI                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fs2p:FS2P_KI");
        FS2P_KD                                 = my_xml_reader.readDoubleValue("ibvs_controller_config:fs2p:FS2P_KD");

        MULTIROTOR_IBVSCONTROLLER_FS2P_KP = ((FS2P_KP) * MULTIROTOR_IBVSCONTROLLER_FS2P_DELTA_KP);
        MULTIROTOR_IBVSCONTROLLER_FS2P_KI = ((FS2P_KI ) * MULTIROTOR_IBVSCONTROLLER_FS2P_KP );
        MULTIROTOR_IBVSCONTROLLER_FS2P_KD = ((FS2P_KD) * MULTIROTOR_IBVSCONTROLLER_FS2P_KP );

        MULTIROTOR_IBVSCONTROLLER_INITVAL_FX_ROLL = my_xml_reader.readDoubleValue("ibvs_controller_config:initval:MULTIROTOR_IBVSCONTROLLER_INITVAL_FX_ROLL");
        MULTIROTOR_IBVSCONTROLLER_INITVAL_FX_YAW  = my_xml_reader.readDoubleValue("ibvs_controller_config:initval:MULTIROTOR_IBVSCONTROLLER_INITVAL_FX_YAW");
        MULTIROTOR_IBVSCONTROLLER_INITVAL_FY      = my_xml_reader.readDoubleValue("ibvs_controller_config:initval:MULTIROTOR_IBVSCONTROLLER_INITVAL_FY");
        MULTIROTOR_IBVSCONTROLLER_INITVAL_FS      = my_xml_reader.readDoubleValue("ibvs_controller_config:initval:MULTIROTOR_IBVSCONTROLLER_INITVAL_FS");
        MULTIROTOR_IBVSCONTROLLER_INITVAL_FD      = ( 1.0/sqrt(MULTIROTOR_IBVSCONTROLLER_INITVAL_FS) );

        MULTIROTOR_IBVSCONTROLLER_SAFETY_DISTANCE_TO_IMAGE_BORDER = my_xml_reader.readDoubleValue("ibvs_controller_config:initval:MULTIROTOR_IBVSCONTROLLER_SAFETY_DISTANCE_TO_IMAGE_BORDER");
        MULTIROTOR_IBVSCONTROLLER_SAFETY_TARGET_IMAGE_AREA = pow((1-2*MULTIROTOR_IBVSCONTROLLER_SAFETY_DISTANCE_TO_IMAGE_BORDER),2);
    }

    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }


    return true;
}

DroneIBVSController::~DroneIBVSController()
{
    close();
    return;
}

void DroneIBVSController::init(std::string configFile)
{
    // Read configs
    readConfigs(configFile);


    //Control mode
    control_mode = MULTIROTOR_IBVSCONTROLLER_INIT_CONTROLMODE;

    //Filters
    pitch_lowpassfilter.setResponseTime(MULTIROTOR_TILT_REFERENCE_CUTOFF_TR);
    pitch_lowpassfilter.enableSaturation( true, -MULTIROTOR_IBVSCONTROLLER_MAX_PITCH, +MULTIROTOR_IBVSCONTROLLER_MAX_PITCH);
    pitch_lowpassfilter.reset();
    roll_lowpassfilter.setResponseTime(MULTIROTOR_TILT_REFERENCE_CUTOFF_TR);
    roll_lowpassfilter.enableSaturation( true, -MULTIROTOR_IBVSCONTROLLER_MAX_ROLL, +MULTIROTOR_IBVSCONTROLLER_MAX_ROLL);
    roll_lowpassfilter.reset();
    dyaw_lowpassfilter.setResponseTime(MULTIROTOR_DYAW_REFERENCE_CUTOFF_TR);
    dyaw_lowpassfilter.enableSaturation( true, -MULTIROTOR_IBVSCONTROLLER_DYAWMAX, +MULTIROTOR_IBVSCONTROLLER_DYAWMAX);
    dyaw_lowpassfilter.reset();
    dz_lowpassfilter.setResponseTime(MULTIROTOR_DALT_REFERENCE_CUTOFF_TR);
    dz_lowpassfilter.enableSaturation( true, -MULTIROTOR_IBVSCONTROLLER_DZMAX, +MULTIROTOR_IBVSCONTROLLER_DZMAX);
    dz_lowpassfilter.reset();

    pid_fx2R.setGains( 	MULTIROTOR_IBVSCONTROLLER_FX2R_KP,
                        MULTIROTOR_IBVSCONTROLLER_FX2R_KI,
                        MULTIROTOR_IBVSCONTROLLER_FX2R_KD);
    pid_fx2R.enableMaxOutput(true,  MULTIROTOR_IBVSCONTROLLER_MAX_ROLL);
    pid_fx2R.enableAntiWindup(true, MULTIROTOR_IBVSCONTROLLER_MAX_ROLL);

    pid_fx2DY.setGains( MULTIROTOR_IBVSCONTROLLER_FX2DY_KP,
                        MULTIROTOR_IBVSCONTROLLER_FX2DY_KI,
                        MULTIROTOR_IBVSCONTROLLER_FX2DY_KD);
    pid_fx2DY.enableMaxOutput(true,  MULTIROTOR_IBVSCONTROLLER_DYAWMAX);
    pid_fx2DY.enableAntiWindup(true, MULTIROTOR_IBVSCONTROLLER_DYAWMAX);

    pid_fy.setGains( 	MULTIROTOR_IBVSCONTROLLER_FY2DZ_KP,
                        MULTIROTOR_IBVSCONTROLLER_FY2DZ_KI,
                        MULTIROTOR_IBVSCONTROLLER_FY2DZ_KD);
    pid_fy.enableMaxOutput(true,  MULTIROTOR_IBVSCONTROLLER_DZMAX);
    pid_fy.enableAntiWindup(true, MULTIROTOR_IBVSCONTROLLER_DZMAX);

    pid_fs.setGains( 	MULTIROTOR_IBVSCONTROLLER_FS2P_KP,
                        MULTIROTOR_IBVSCONTROLLER_FS2P_KI,
                        MULTIROTOR_IBVSCONTROLLER_FS2P_KD);
    pid_fs.enableMaxOutput(true,  MULTIROTOR_IBVSCONTROLLER_MAX_PITCH);
    pid_fs.enableAntiWindup(true, MULTIROTOR_IBVSCONTROLLER_MAX_PITCH);

    pid_fD.setGains( 	MULTIROTOR_IBVSCONTROLLER_FD2P_KP,
                        MULTIROTOR_IBVSCONTROLLER_FD2P_KI,
                        MULTIROTOR_IBVSCONTROLLER_FD2P_KD);
    pid_fD.enableMaxOutput(true,  MULTIROTOR_IBVSCONTROLLER_MAX_PITCH);
    pid_fD.enableAntiWindup(true, MULTIROTOR_IBVSCONTROLLER_MAX_PITCH);


    //Init
    resetValues();

    // Constant to calculate distance2target from fxs, fys, fDs
    C_fx2Dy  = MULTIROTOR_FRONTCAM_C_fx2Dy;
    C_fx2DY  = MULTIROTOR_FRONTCAM_C_fx2DY;
    C_fy2Dz  = MULTIROTOR_FRONTCAM_C_fy2Dz;
    C_fD2Dx  = MULTIROTOR_FRONTCAM_C_fD2Dx;
    C_DY2Dfx = MULTIROTOR_FRONTCAM_C_DY2Dfx;
    C_DP2Dfy = MULTIROTOR_FRONTCAM_C_DP2Dfy;
    C_ObjVisArea = MULTIROTOR_IBVSCONTROLLER_TARGET_INIT_SIZE;
    // estimated distance to target
    Dxs = 0.0;
    Dys = 0.0;
    Dzs = 0.0;
    DYs = 0.0;

    // telemetry
    yaw_t   = 0.0;
    pitch_t = 0.0;
    roll_t  = 0.0;
    // centroid reference
    yaw_cr = 0.0;
    pitch_cr = 0.0;
    roll_cr = 0.0;

    //End
    return;
}

bool DroneIBVSController::close()
{
    return true;
}

bool DroneIBVSController::resetValues()
{
    control_mode = MULTIROTOR_IBVSCONTROLLER_INIT_CONTROLMODE; //Speed control mode as default!!

    pitch_lowpassfilter.reset();
    roll_lowpassfilter.reset();
    dyaw_lowpassfilter.reset();
    dz_lowpassfilter.reset();

    pid_fx2R.reset();
    pid_fx2DY.reset();
    pid_fy.reset();
    pid_fs.reset();
    pid_fD.reset();

    // co ~ command outputs
    pitchco_hf = 0.0, rollco_hf = 0.0, dyawco_hf = 0.0, dzco_hf = 0.0;
    pitchco = 0.0, rollco = 0.0, dyawco = 0.0, dzco = 0.0;
    setNavCommandToZero();

    // image feature measurements and references
    fxci     = MULTIROTOR_IBVSCONTROLLER_INITVAL_FX_ROLL;
    fxci_yaw = MULTIROTOR_IBVSCONTROLLER_INITVAL_FX_YAW;
    fyci     = MULTIROTOR_IBVSCONTROLLER_INITVAL_FY;
    fsci     = MULTIROTOR_IBVSCONTROLLER_INITVAL_FS;
    fDci     = MULTIROTOR_IBVSCONTROLLER_INITVAL_FD;
    fxs = fxci, fys = fyci, fss = fsci,  fDs = fDci;
    Dfx = 0.0, Dfy = 0.0, Dfs = 0.0, DfD = 0.0;
    Dfx_4DYC = 0.0, Dfx_4DyC = 0.0, Dfy_4DzC = 0.0, DfD_4DxC = 0.0;

    yaw_cr   = yaw_t;

    return true;
}

bool DroneIBVSController::startValues()
{
    if(target_is_on_frame)
        setControlModeVal(PERSON_FOLLOWING);
    else
        setControlModeVal(PERSON_NOT_ON_FRAME);
    return true;
}

bool DroneIBVSController::stopValues()
{
    setNavCommandToZero();
    setControlModeVal(CTRL_NOT_STARTED);
}

bool DroneIBVSController::start()
{
    controller_is_started = true;
    return true;
}

void DroneIBVSController::setNavCommand(float roll, float pitch, float dyaw, float dz, double time)
{
    //Set time
    if(time<0.0)
    {
        double actualTime = ros::Time::now().toSec();
        droneNavCommandMsg.time=actualTime;
    }
    else
    {
        droneNavCommandMsg.time=time;
    }
    //Set others
    droneNavCommandMsg.dyaw=dyaw;
    droneNavCommandMsg.dz=dz;
    droneNavCommandMsg.pitch=pitch;
    droneNavCommandMsg.roll=roll;



    pitchco=droneNavCommandMsg.pitch;
    rollco=droneNavCommandMsg.roll;
    dyawco=droneNavCommandMsg.dyaw;
    dzco=droneNavCommandMsg.dz;

    return;

}

void DroneIBVSController::setNavCommandToZero(void)
{
    //set
    setNavCommand(0.0,0.0,0.0,0.0,-1.0);
    //End
    return;
}


droneMsgsROS::droneNavCommand DroneIBVSController::getNavCommand()
{
    return droneNavCommandMsg;
}


bool DroneIBVSController::setControlModeVal(controlMode mode)
{
    if (control_mode != mode) {

        switch (mode) {
        case CTRL_NOT_STARTED:
            break;
        case PERSON_FOLLOWING:
            // independetly of the prior control_mode do...
            resetValues();
            break;
        case PERSON_NOT_ON_FRAME:
            break;
        default:
            return false;
            break;
        }

        control_mode = mode;
    }
    return true;
}

DroneIBVSController::controlMode DroneIBVSController::controlModeFromInt( int controlMode_int) {
    DroneIBVSController::controlMode control_mode_enum = MULTIROTOR_IBVSCONTROLLER_INIT_CONTROLMODE;

    switch (controlMode_int) {
    case 0: //Default
        control_mode_enum= MULTIROTOR_IBVSCONTROLLER_INIT_CONTROLMODE;
        break;
    case 1:
        control_mode_enum= CTRL_NOT_STARTED;
        break;
    case 2:
        control_mode_enum= PERSON_FOLLOWING;
        break;
    case 3:
        control_mode_enum= PERSON_NOT_ON_FRAME;
        break;
    default:
        control_mode_enum= MULTIROTOR_IBVSCONTROLLER_INIT_CONTROLMODE;
        break;
    }

    return control_mode_enum;
}

int DroneIBVSController::controlModeToInt( controlMode controlMode_enum) {
    int controlMode_int = 1;

    switch (controlMode_enum) {
    case CTRL_NOT_STARTED:
        controlMode_int = 1;
        break;
    case PERSON_FOLLOWING:
        controlMode_int = 2;
        break;
    case PERSON_NOT_ON_FRAME:
        controlMode_int = 3;
        break;
    default:
        controlMode_int = 1;
        break;
    }

    return controlMode_int;
}

DroneIBVSController::controlMode DroneIBVSController::getControlMode()
{

    controlMode current_control_mode;
    //if ( controllerMutex.lock() ) {
        current_control_mode = control_mode;
    //	controllerMutex.unlock();
    //}
    return current_control_mode;
}
// END: ***** Get/Accessors Controller functions *****

bool DroneIBVSController::run()
{
    bool run_successful = true;

    updateDistanceToTarget();

    float diff_yaw = 0.0;
    int   sign_diff_yaw = 0;
    switch (control_mode) {
    case CTRL_NOT_STARTED:
        pitchco = 0.0;
        rollco  = 0.0;
        dyawco  = 0.0;
        dzco    = 0.0;
        run_successful = false;
        break;
    case PERSON_FOLLOWING:
        // update yaw_cr, diff_yaw = yaw_t-yaw_cr;
        sign_diff_yaw = (cos(yaw_t)*sin(yaw_cr) - cos(yaw_cr)*sin(yaw_t))>0 ? +1 : -1;
        diff_yaw = sign_diff_yaw*acos(cos(yaw_t)*cos(yaw_cr) + sin(yaw_t)*sin(yaw_cr));
        if ( fabs(diff_yaw)>(25.0*M_PI/180.0)) {
            yaw_cr   = yaw_t;
            diff_yaw = 0.0;
        }

        // Calculate image feature tracking error
        Dfx      = fxci - fxs;
        Dfy      = fyci - fys;
        Dfs      = fsci - fss;
        DfD      = fDci - fDs;
        Dfx_4DYC = Dfx;
        Dfx_4DyC = Dfx + C_DY2Dfx*diff_yaw;
        Dfy_4DzC = Dfy + C_DP2Dfy*(pitch_t - pitch_cr);
        DfD_4DxC = DfD;
        fxs_4DyC = fxci - Dfx_4DyC;
        fxs_4DYC = fxci - Dfx_4DYC;
        fys_4DzC = fyci - Dfy_4DzC;
        fDs_4DxC = fDci - DfD_4DxC;

        // set PID input, and calculate ouput
        pid_fx2R.setReference(fxci);
        pid_fx2R.setFeedback( fxs_4DyC );
        rollco_hf = pid_fx2R.getOutput();

        pid_fx2DY.setReference(fxci_yaw);
        pid_fx2DY.setFeedback( fxs_4DYC );
        dyawco_hf = pid_fx2DY.getOutput();

        pid_fy.setReference(fyci);
        pid_fy.setFeedback( fys_4DzC );
        dzco_hf = pid_fy.getOutput();

        pid_fs.setReference(fsci);          // out-dated
        pid_fs.setFeedback( fss );          // out-dated
//        pitchco_hf = pid_fs.getOutput();  // out-dated

        pid_fD.setReference(fDci);
        pid_fD.setFeedback( fDs_4DxC );
        pitchco_hf = pid_fD.getOutput();

        // (low pass) filter the command outputs
        pitch_lowpassfilter.setInput(pitchco_hf);
        pitchco = pitch_lowpassfilter.getOutput();
        roll_lowpassfilter.setInput(rollco_hf);
        rollco = roll_lowpassfilter.getOutput();
        dyaw_lowpassfilter.setInput(dyawco_hf);
        dyawco = dyaw_lowpassfilter.getOutput();
        // Uncomment to switch off yaw control
//        dyawco = 0.0;
        dz_lowpassfilter.setInput(dzco_hf);
        dzco = dz_lowpassfilter.getOutput();

        // Send commands to the multirotor
        setNavCommand(rollco,pitchco,dyawco,dzco,-1.0);
        run_successful = true;
        break;
    case PERSON_NOT_ON_FRAME:
        // HYP_JP: I think that (HOVER == setNavCommandToZero());
        pitchco = 0.0;
        rollco  = 0.0;
        dyawco  = 0.0;
        dzco    = 0.0;
        setNavCommandToZero();
        run_successful = true;
        break;
    default:
        run_successful = false;
        break;
    }
    return run_successful;
}

bool DroneIBVSController::boundingBox2ImageFeatures( const int bb_x, const int bb_y, const int bb_width, const int bb_height,
                                float &fx, float &fy, float &fs, float &fD, const bool target_is_on_frame_in )
{

    bool error_ocurred = false;
    if ( (bb_width == 0) || (bb_height == 0) || (!target_is_on_frame_in) ) {
        error_ocurred = true;   // degenerate bounding box
        return error_ocurred;
    }

    // [fx]horizontal and [fy]vertical position of the centroid on the image
    fx = ( (float) bb_x + ((float) bb_width)/2.0 )/MULTIROTOR_FRONTCAM_RESOLUTION_WIDTH;
    fy = ( (float) bb_y + ((float)bb_height)/2.0 )/MULTIROTOR_FRONTCAM_RESOLUTION_HEIGHT;

    // [fs]size and [fD]inverse sqrt of size of object in the image
    fs = (((float)bb_width)/MULTIROTOR_FRONTCAM_RESOLUTION_WIDTH)*(((float)bb_height)/MULTIROTOR_FRONTCAM_RESOLUTION_HEIGHT);
    fD = 1.0/sqrt( (float)fs );

    return error_ocurred;
}

void DroneIBVSController::getImFeatReferences(float &fxci_out, float &fyci_out, float  &fsci_out, float &fDci_out)
{

    fxci_out = fxci;
    fyci_out = fyci;
    fsci_out = fsci;
    fDci_out = fDci;

    return;
}

void DroneIBVSController::getImFeatFeedback2PIDs( float &fxs_4DyC_out, float &fxs_4DYC_out, float &fys_4DzC_out, float &fDs_4DxC_out) {

    if ( controller_is_started ) {
        fxs_4DyC_out = fxs_4DyC;
        fxs_4DYC_out = fxs_4DYC;
        fys_4DzC_out = fys_4DzC;
        fDs_4DxC_out = fDs_4DxC;
    } else {
        fxs_4DyC_out = fxs;
        fxs_4DYC_out = fxs;
        fys_4DzC_out = fys;
        fDs_4DxC_out = fDs;
    }

    return;
}

bool DroneIBVSController::setImFeatMeasurements( const float fxs_in,  const float fys_in,  const float fss_in,  const float fDs_in, const int bb_width_in, const int bb_heigth_in) {

    bool error_ocurred = false;
    // podría comprobar que todos los valores estan entre 0 y 1 (fD solo >0.0), de momento no lo voy a hacer

    //internal value

        fxs = fxs_in;
        fys = fys_in;
        fss = fss_in;
        fDs = fDs_in;
        bb_width  = bb_width_in;
        bb_heigth = bb_heigth_in;


    return error_ocurred;
}

void DroneIBVSController::setTargetIsOnFrame( const bool target_is_on_frame_in) {


        target_is_on_frame = target_is_on_frame_in;
        if ( controller_is_started ) {
            if ( target_is_on_frame ) {
                setControlModeVal( PERSON_FOLLOWING );
            } else {
                setControlModeVal( PERSON_NOT_ON_FRAME );
            }
        }

}

float DroneIBVSController::distanceToTarget( const float fD) {

    float depth;
    depth = sqrt(MULTIROTOR_FRONTCAM_ALPHAX*MULTIROTOR_FRONTCAM_ALPHAY*MULTIROTOR_IBVSCONTROLLER_TARGET_INIT_SIZE/(MULTIROTOR_FRONTCAM_RESOLUTION_WIDTH*MULTIROTOR_FRONTCAM_RESOLUTION_HEIGHT)) * fD;

    return depth;

}

void DroneIBVSController::setTelemetryAttitude_rad( double yaw_t_in, double pitch_t_in, double roll_t_in) {
    yaw_t = yaw_t_in;
    pitch_t = pitch_t_in;
    roll_t = roll_t_in;
}


bool DroneIBVSController::setImFeatReferencesFromDPos( float Dxc, float Dyc, float Dzc, float DYc) {

    bool forced_safety_image_area = false;

    float Dfxci, Dfyci, DfDci; // Dfsci
    Dfxci = Dyc/C_fx2Dy + DYc/C_fx2DY;
    Dfyci = Dzc/C_fy2Dz;
    DfDci = Dxc/C_fD2Dx;

    // MULTIROTOR_IBVSCONTROLLER_SAFETY_DISTANCE_TO_IMAGE_BORDER
    // MULTIROTOR_IBVSCONTROLLER_SAFETY_TARGET_IMAGE_AREA
    float fxci_aux = fxci + Dfxci;
    float safety_distance_from_center = (0.5-MULTIROTOR_IBVSCONTROLLER_SAFETY_DISTANCE_TO_IMAGE_BORDER-bb_width/MULTIROTOR_FRONTCAM_RESOLUTION_WIDTH);
    float sign = (fxci_aux-0.5) > 0? 1.0 : -1.0;
    if ( safety_distance_from_center > 0 ) {
        if ( sign*(fxci_aux-0.5) > safety_distance_from_center ) {
            // saturate
            fxci = 0.5 + sign*safety_distance_from_center;
            forced_safety_image_area = true;
        } else {
            // don't saturate
            fxci = fxci_aux;
        }
    } else {
        fxci = 0.5;
        forced_safety_image_area = true;
    }

    float fyci_aux = fyci + Dfyci;
    safety_distance_from_center = (0.5-MULTIROTOR_IBVSCONTROLLER_SAFETY_DISTANCE_TO_IMAGE_BORDER-bb_heigth/MULTIROTOR_FRONTCAM_RESOLUTION_HEIGHT);
    sign = (fyci_aux-0.5) > 0? 1.0 : -1.0;
    if ( safety_distance_from_center > 0 ) {
        if ( sign*(fyci_aux-0.5) > safety_distance_from_center ) {
            // saturate
            fyci = 0.5 + sign*safety_distance_from_center;
            forced_safety_image_area = true;
        } else {
            // don't saturate
            fyci = fyci_aux;
        }
    } else {
        fyci = 0.5;
        forced_safety_image_area = true;
    }

    float fDci_aux = fDci + DfDci;
    float fsci_aux = 1/(fDci_aux*fDci_aux);
    if ( fsci_aux > MULTIROTOR_IBVSCONTROLLER_SAFETY_TARGET_IMAGE_AREA ) {
        // saturate
        fDci_aux = 1/sqrt(MULTIROTOR_IBVSCONTROLLER_SAFETY_TARGET_IMAGE_AREA);
        fDci = fDci_aux;
        forced_safety_image_area = true;
    } else {
        // don't saturate
        fDci = fDci_aux;
    }

    return forced_safety_image_area;
}


void DroneIBVSController::updateDistanceToTarget() {

    // TODO_JP: What happens if fDs == 0 (event which I believe impossible??
    // TODO_JP: Are C_[...] constants always >0 ?
    Dxs = C_fD2Dx*fDs;
    // TODO_JP: I am not yet sure if we want to make this correction on real time
    C_fx2Dy = MULTIROTOR_FRONTCAM_C_fx2Dy * (Dxs/MULTIROTOR_IBVSCONTROLLER_INIT_DEPTH);
    C_fy2Dz = MULTIROTOR_FRONTCAM_C_fy2Dz * (Dxs/MULTIROTOR_IBVSCONTROLLER_INIT_DEPTH);
    Dys = C_fx2Dy*(fxs - 0.5);
    Dzs = C_fy2Dz*(fys - 0.5);
    DYs = C_fx2DY*(fxs - 0.5);
}

std::string DroneIBVSController::controlMode2String() {

    switch( control_mode ) {
    case DroneIBVSController::CTRL_NOT_STARTED:
        return ("stopped");
        break;
    case DroneIBVSController::PERSON_FOLLOWING:
        return ("TOF_IBVS");
        break;
    case DroneIBVSController::PERSON_NOT_ON_FRAME:
        return ("TNOF_IBVS");
        break;
    default:
        return ("unknown_IBVS_control_mode");
        break;
    }
}

